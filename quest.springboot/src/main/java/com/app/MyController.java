package com.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/home")
public class MyController {
	
	private IUserRepoService userService;
	@Autowired
	public void setUserService(IUserRepoService userService) {
		this.userService = userService;
	}
	@RequestMapping(value="")
	public ModelAndView firstpage() {
		return new ModelAndView("home");
		
	}
	@RequestMapping(value = "/addUser")
	public ModelAndView addUser() {
		ModelAndView model = new ModelAndView("addUser");
		return model;
	}
	@RequestMapping(value = "/listUser",method=RequestMethod.GET)
	public ModelAndView listUser() {
		ModelAndView model = new ModelAndView("listUser");
		model.addObject("list", userService.listAllUsers());
		return model;
	}
	@RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
    public ModelAndView deleteUsers(@PathVariable String id) {
	   	userService.deleteUser(id);
        return new ModelAndView("redirect:/home");
    }
	/*@RequestMapping(value = "/add")
	public ModelAndView add() {
		ModelAndView model = new ModelAndView("sample");
		return model;
	}*/
	
	@RequestMapping(value="/add", method = RequestMethod.POST)
    public ModelAndView userRegister(@ModelAttribute("user")User user){
    	ModelAndView model = new ModelAndView("add");
    	if(user!=null){
    	userService.saveUser(user);
    	model.addObject("warning", "User Registration Success");
    	
    	}
    	else{
    		model.addObject("danger","Something Going Bad" );
    		
    	}
    	return new ModelAndView("redirect:/home");
    }
	
}
