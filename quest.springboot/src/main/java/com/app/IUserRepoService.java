package com.app;

import org.springframework.stereotype.Component;

public interface IUserRepoService {

	   public Iterable<User> listAllUsers();

	   public User getUserById(String id);

	   public User saveUser(User user);
	    
	   public void deleteUser(String id);
	   
}
