package com.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements IUserRepoService{

	private IUserRepo userRepo;
	
	//Spring Setter Injection
	@Autowired
	public void setUserRepo(IUserRepo userRepo) {
		this.userRepo = userRepo;
	}

	@Override
	public Iterable<User> listAllUsers() {
		return userRepo.findAll();
	}

	@Override
	public User getUserById(String id) {
		return userRepo.findOne(id);
	}

	@Override
	public User saveUser(User user) {
		return userRepo.save(user);
	}
	
	@Override
	public void deleteUser(String id) {
		
		 userRepo.delete(id);
		
	}

	
}
