package com.app;

import org.springframework.data.repository.CrudRepository;

public interface IUserRepo  extends CrudRepository<User, String>  {

}
